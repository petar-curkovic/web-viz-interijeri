source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Rails
gem 'rails',             '~> 5.1.4'

# Database
gem 'pg',                '~> 0.18'

# Application Support
gem 'mina-infinum', require: false
gem 'puma',              '~> 3.7'

# CSS
gem 'bootstrap-sass',    '~> 3.3.7'
gem 'font-awesome-sass', '~> 4.7.0'
gem 'sass-rails',        '~> 5.0'

# JavaScript
gem 'jquery-rails'
# gem 'uglifier'
gem 'therubyracer', group: :staging

gem 'slim'

# See https://github.com/rails/execjs#readme for more supported runtimes

# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]

  gem 'pry'
  gem 'pry-byebug',                 '1.3.3'
end

group :development do
  gem 'better_errors', '~> 2.0.0'
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
