module ApplicationHelper
  def nav_link_class(path)
    return if request.path != path

    'nav-active'
  end
end
