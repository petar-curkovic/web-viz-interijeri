//= require jquery
//= require bootstrap

//= require nav
//= require custom

//= require effects/masonry.pkgd.min
//= require effects/imagesloaded
//= require effects/classie
//= require effects/AnimOnScroll
//= require effects/modernizr.custom
//= require html5shiv
