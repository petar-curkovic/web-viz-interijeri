require 'mina/rails'
require 'mina/git'
require 'mina/secrets'
require 'mina/infinum'

set :application_name, 'VIZ Interijeri'
set :repository, 'https://petar-curkovic@bitbucket.org/petar-curkovic/web-viz-interijeri.git'
set :user, 'vizinter'
set :keep_releases, 1

task :production do
  set :domain, 'vizinterijeri.hr'
  set :deploy_to, '/home/vizinter/vizinterijeri.hr'
  set :rails_env, 'production'
  set :branch, 'master'
  set :port, 22
end

desc 'Deploys the current version to the server.'
task :deploy do
  invoke :'git:ensure_pushed'

  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    on :launch do
      in_path(fetch(:current_path)) do
        comment %(Restarting application)
        command %(mkdir -p tmp/)
        command %(touch tmp/restart.txt)
      end
    end
  end
end
