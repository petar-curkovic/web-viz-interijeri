Rails.application.routes.draw do
  get :home,    to: 'public#home'
  get :contact, to: 'public#contact'
  get :about,   to: 'public#about'
  get :gallery, to: 'public#gallery'

  root to: 'public#about'
end
